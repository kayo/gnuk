default: build

config:
	cd src && ./configure --vidpid=234b:0000 --enable-certdo --enable-keygen

ifeq (,$(wildcard src/Makefile))
build:
	@echo "You need configure project first."
	@echo "Use command: make config"
else
build:
	cd src && make
	@echo "Now you can attach your token and flash firmware using OpenOCD and JTAG/SWD tool."
	@echo "Just type: make flash"
endif

ifeq (,$(wildcard src/build/gnuk.elf))
flash:
ifeq (,$(wildcard src/Makefile))
	@echo "First you need configure and build project."
	@echo "Try use: make config && make"
else
	@echo "First you need build project."
	@echo "Try use: make"
endif
else
flash:
	cd src && make flash
endif

clean:
	cd src && make distclean
